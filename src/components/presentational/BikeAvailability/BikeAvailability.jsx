import styled from 'styled-components'
import React from 'react'
import Wrapper from 'components/shared/Wrapper/Wrapper'
import TextInline from '../../shared/TextInline/TextInline'

const BikeAvailabilityWrapper = styled(Wrapper)`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 100%;
  margin-bottom: 30px;

  .percent {
    font-size: 40px;
    font-weight: 500;
  }
`

const BikeAvailability = ({
  percent,
  thumbIcon,
}) => (
  <BikeAvailabilityWrapper>
    <TextInline>Availability:{thumbIcon}</TextInline>
    <TextInline className='percent'>{percent}</TextInline>
  </BikeAvailabilityWrapper>
)

export default BikeAvailability
