import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'http://api.citybik.es/',
})

const BikeAPI = {
  getNetworks() {
    return axiosInstance.get('/v2/networks?fields=id,company,href,location')
  },
  fetchBikeNetworkDetails(url) {
    return axiosInstance.get(`${url}`)
  }
}

export default BikeAPI
